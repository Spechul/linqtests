﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;
using BenchmarkDotNet.Running;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetFrameworkApp
{
    public class Program
    {
        const int take  = 1000000;
        const int total = 10000000;
        public class Item
        {
            public string Name { get; set; }
            public int Id { get; set; }
        }

        public static IEnumerable<Item> GenerateItems()
        {
            var random = new Random();
            while (true)
            {
                yield return new Item { Id = random.Next(), Name = random.Next().ToString() };
            }
        }

        public static void Main(string[] args)
        {
            var summary1 = BenchmarkRunner.Run<TakeBefore>();
            var summary = BenchmarkRunner.Run<TakeAfter>();
            Console.ReadLine();
        }

        public class TakeBefore
        {
            List<Item> _list;

            public TakeBefore()
            {
                _list = GenerateItems().Take(total).ToList();
            }

            [Benchmark]
            public void Run()
            {
                var list1 = _list.Take(take).Select(x => x.Id).ToList();
            }
        }

        public class TakeAfter
        {
            List<Item> _list;
            public TakeAfter()
            {
                _list = GenerateItems().Take(total).ToList();
            }

            [Benchmark]
            public void Run()
            {
                var list1 = _list.Select(x => x.Id).Take(take).ToList();
            }
        }
    }
}
